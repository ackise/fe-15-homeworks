


var Hamburger = function Hamburger(size,stuffing){
    if(arguments[0] === undefined){
        throw new HamburgerException('no size given')
    } else if(arguments[1] === undefined){
        throw new HamburgerException('no stuffing was given')
    }else if(arguments[0].type !=='size') {
        throw new HamburgerException("Invalid size")
    }else if(arguments[1].type !=='stuffing') {
        throw new HamburgerException("Invalid stuffing")
    } else {
        this.size = size;
        this.stuffing = stuffing;
        this.toppings = [];
    }
}





Hamburger.SIZE_SMALL = {price:50,calories:20,type:"size"}
Hamburger.SIZE_LARGE = {price:100,calories:40,type:"size"}
Hamburger.STUFFING_CHEESE = {price:10,calories:20,type:"stuffing"}
Hamburger.STUFFING_SALAD = {price:20,calories:5,type:"stuffing"}
Hamburger.STUFFING_POTATO = {price:15,calories:10,type:"stuffing"}
Hamburger.TOPPING_MAYO = {price:20,calories:50}
Hamburger.TOPPING_SPICE = {price:15,calories:0}





Hamburger.prototype.addTopping = function toppings (topping){
    if(!this.toppings.includes(topping)){
        return this.toppings.push(topping)
    }else {
        throw new HamburgerException("duplicate topping")
    }
}

Hamburger.prototype.removeTopping = function (topping) {
    return this.toppings = this.toppings.filter(function(a){
       return a !== topping
    })
}

Hamburger.prototype.getToppings = function (topping){
    return this.toppings;
}
Hamburger.prototype.getSize = function (size){
    return this.size;
}

Hamburger.prototype.getStuffing = function (stuffing){
    return this.stuffing;
}

Hamburger.prototype.calculatePrice = function () {
    return this.size.price + this.stuffing.price + this.toppings.reduce(function (acc, val) {
       return  acc + val.price
    }, 0)
}
Hamburger.prototype.calculateCalories = function () {
    return this.size.calories + this.stuffing.calories + this.toppings.reduce(function (acc, val) {
        return  acc + val.calories
     }, 0)
}

function HamburgerException(message) {
    this.message = message;
    this.name = 'HamburgerException';
}




var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log("Calories: %f", hamburger.calculateCalories());
console.log("Price: %f", hamburger.calculatePrice());
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log("Price with sauce: %f", hamburger.calculatePrice());
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); 
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); 




// Errors
// var h2 = new Hamburger(Hamburger.SIZE_SMALL);
// var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE); 


// var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// hamburger.addTopping(Hamburger.TOPPING_MAYO);
// hamburger.addTopping(Hamburger.TOPPING_MAYO); 

