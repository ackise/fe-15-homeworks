let easyBtn = document.createElement('button')
easyBtn.innerText = "EASY"
let middleBtn = document.createElement('button')
middleBtn.innerText = "MIDDLE"
let hardBtn = document.createElement('button')
hardBtn.innerText = "HARD"
document.querySelector('body').prepend(easyBtn, middleBtn, hardBtn)





class createTable{
constructor(parent,cols,rows){
    const elem = document.querySelector('#elem');
    let table = document.createElement('table');

    for ( let i = 0;i<rows; i++){
        let tr = document.createElement('tr');
        for (let j =0; j<cols;j++){
            let td = document.createElement('td');
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    parent.appendChild(table);
}
} 

class Game {
    static EASY = {name: "easy", time: 1500};
    static MIDDLE = {name: "middle", time: 1000};
    static HARD = {name: "hard", time: 500};
    constructor(difficulty){
        this.difficulty = difficulty;
        this.humanScore = 0;
        this.compScore = 0;
        this.interval = 0;
    }
    start (){
        this.removebuttons();
        this.scoreCount();
        this.randomCell();
        if(this.interval === 0) {
            this.interval = setInterval(this.randomCell.bind(this), this.difficulty.time)
        }
    }
    removebuttons(){
        easyBtn.style.display = 'none'
        middleBtn.style.display = 'none'
        hardBtn.style.display = 'none'
        }
   
    randomCell(){
        let row = Math.floor(Math.random() * (10 - 1 + 1) + 1)
        let column = Math.floor(Math.random() * (10 - 1 + 1) + 1)
        let cell = document.querySelector(`tr:nth-child(${row}) td:nth-child(${column})`);
        console.log(cell)
        if(!(cell.classList.contains('userpoint') || cell.classList.contains('computerpoint'))){
            cell.classList.add('play')
        }
            cell.addEventListener('click', () => {
                if(cell.classList.contains('play')) {
                cell.classList.remove('play')
                cell.classList.add('userpoint')
                this.humanScore++;
                this.gameControlRemove()
                this.scoreCount()
            }
        })
        setTimeout(() =>{
            if(cell.classList.contains('play')) {
                cell.classList.remove('play')
                cell.classList.add('computerpoint')
                this.compScore++;
                this.gameControlRemove()
                this.scoreCount()
            }
        }, this.difficulty.time)
        if(this.humanScore > 50 || this.compScore > 50) {
            this.end()
        }
    
    }
    scoreCount(){
        let userScore = document.createElement('span');
        let computerScore = document.createElement('span');
        userScore.innerText = "user:" + `${this.humanScore}`
        computerScore.innerText = "comp:" + `${this.compScore}`
        document.querySelector('body').append(userScore,computerScore)

    }
    gameControlRemove() {
        document.querySelectorAll('span').forEach(e => e.remove())
    }

    end() {
        clearInterval(this.interval)
        this.interval = 0; 
        let reloadButton = document.createElement('button');
        reloadButton.classList.add('reloadBtn')
        reloadButton.innerText = 'New game';
        reloadButton.addEventListener('click',()=>{
        document.location.reload(true);
        })
        document.querySelector('body').append(reloadButton);
        let winnerText = document.createElement('h3')
        if(this.humanScore > 50) {
            winnerText.innerText = 'YOU WIN! Reload page for new game'
            document.querySelector('body').append(winnerText)
        } else {
            winnerText.innerText = 'YOU LOSE. Reload page for new game'
            document.querySelector('body').append(winnerText)
        }
    }

}

// Start Game
easyBtn.addEventListener('click', () => {
    let gameTable = new createTable(elem,10,10); 
    let newGame = new Game(Game.EASY)
    newGame.start()
});

middleBtn.addEventListener('click', () => {
    let gameTable = new createTable(elem,10,10);
    let newGame = new Game(Game.MIDDLE)
    newGame.start()
    

})

hardBtn.addEventListener('click', () => {
    let gameTable = new createTable(elem,10,10);
    let newGame = new Game(Game.HARD)
    newGame.start()
})









