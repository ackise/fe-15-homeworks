import React from 'react';
import CardWrap from '../../components/CardWrap/CardWrap'

const Favorites = (props) =>{
    const {stuff} = props
    return (
        <>
            <h2>Favorites products</h2>
            <CardWrap stuff={stuff}></CardWrap>
        </>
    )
}

export default Favorites
