import React from 'react';
import CardWrap from '../../components/CardWrap/CardWrap'

const Cart = (props) =>{
    const {stuff} = props
    return (
        <>
            <h2>Producrs in cart</h2>
            <CardWrap stuff={stuff}></CardWrap>
        </>
    )
}

export default Cart
