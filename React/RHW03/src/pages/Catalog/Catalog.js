import React from 'react';
import CardWrap from '../../components/CardWrap/CardWrap'

const Catalog = (props) =>{
    const {stuff} = props
    return (
        <>
            <h2>Our products</h2>
            <CardWrap stuff={stuff}></CardWrap>
        </>
    )
}

export default Catalog