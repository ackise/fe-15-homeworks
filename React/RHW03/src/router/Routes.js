import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Favorites from '../pages/Favorites/Favorites';
import Cart from '../pages/Cart/Cart'
import Catalog from '../pages/Catalog/Catalog';

const Routes = (props) => {
    const {stuff} = props
    const stuffCart = stuff.filter(e=>localStorage.getItem(`cart${e.article}`))
    const stuffFavorites = stuff.filter(e=>localStorage.getItem(`favorites${e.article}`))
    return(
        <>
            <Switch>
                <Route exact path='/' render={() => <Redirect to='/catalog' />} />
                <Route exact path='/catalog' render ={()=> <Catalog stuff={stuff}></Catalog>}></Route>
                <Route exact path='/cart' render={()=> <Cart stuff={stuffCart}></Cart>} ></Route>
                <Route exact path='/favorites' render={()=> <Favorites stuff={stuffFavorites}></Favorites>} ></Route>
            </Switch>
        </>
    )
}
export default Routes