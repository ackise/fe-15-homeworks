import React, { useState, useEffect} from 'react';
import './App.css';
import CardWrap from './components/CardWrap/CardWrap';
import Header from './components/Header/Header';
import Routes from './router/Routes';

const App = () => {
  const [stuff,setStuff] = useState(null)
  useEffect(()=>{
        fetch('/stuff.json')
          .then(res => res.json())
          .then(data => setStuff(data)
          ) 
    })
    return (

      <div className="App">
        <Header/>
        {stuff && <Routes stuff={stuff}></Routes>}
        
      </div>
    );  
}

export default App;

