import React, {useState } from 'react'
import './Card.scss';
import Button from '../Btn/Button'
import Modal from '../Modal/Modal'


const  Card = (props) => {
    const [inFavorites,setInFavorites] = useState(false)
    const [inCart,setInCart] = useState(false)
    const [firstModal,setFirstModal] = useState({
            isOpen: false,
            header: 'Success',
            body: 'Item was added to cart',
            type:'first-modal',
            closeButton: true,
            activeMiodal: null,
            actions: [
              <Button
              key={0}
              text={'OK'}
              backgroundColor={'whitesmoke'}
              color={'#44a8f2'}
              onClick= {()=> closeFirstModal()}
          />,
            ]
    })
    const[secondModal,setSecondModal] = useState({
        isOpen: false,
        header:'Do you want delete this product from cart?',
        body:'Once you delete this product, it won’t be available in cart.Are you sure you want to delete it?',
        type:'second-modal',
        closeButton: true,
        activeMiodal: null,
        actions: [
          <Button
          key={0}
          text={'Ok'}
          backgroundColor={'whitesmoke'}
          onClick={()=> deleteFromCart()}
          color = {'red'}
          />,
          <Button
              key={1}
              text={'Cancel'}
              backgroundColor={'whitesmoke'}
              onClick= {()=> closeSecondModal()}
              color = {'red'}
          />
          ]
    })

    const addToCart = (id)=> {
        localStorage.setItem(`cart${id}`, true)
      }
      const deleteFromCart = ()=> {
        setInCart(!inCart)
        closeSecondModal()   
     if(!localStorage.getItem(`cart${props.e.article}`)) {
     } else {
         localStorage.removeItem(`cart${props.e.article}`)
     }
}
    const openFirstModal = ()=> {setFirstModal(
        {...firstModal,
        isOpen: true,
        activeMiodal: 'firstModal'
        }
        )}
    const closeFirstModal = ()=>{setFirstModal(
        {...firstModal,
        isOpen: false}
        )}
    const openSecondModal = ()=> {setSecondModal(
            {...secondModal,
            isOpen: true,
            activeMiodal: 'secondModal'
            }
            )}
    const closeSecondModal = ()=>{setSecondModal(
            {...secondModal,
            isOpen: false}
            )}    
       
        const modalochka = firstModal.activeMiodal && <Modal 
        isOpen= {firstModal.isOpen} 
        onClose = {(e) => setFirstModal({...firstModal,isOpen: false})} 
        header = {firstModal.header}
        body = {firstModal.body}
        actions = {firstModal.actions}
        closeButton = {firstModal.closeButton}
        type = {firstModal.type}
        ></Modal>
        const modalochka2 = secondModal.activeMiodal && <Modal 
        isOpen= {secondModal.isOpen} 
        onClose = {(e) => setSecondModal({...secondModal,isOpen: false})} 
        header = {secondModal.header}
        body = {secondModal.body}
        actions = {secondModal.actions}
        closeButton = {secondModal.closeButton}
        type = {secondModal.type}
        ></Modal>
        const {e} = props

        const favorites = ()=> {
                setInFavorites(!inFavorites)    
             if(!localStorage.getItem(`favorites${props.e.article}`)) {
                 localStorage.setItem(`favorites${props.e.article}`, true)
             } else {
                 localStorage.removeItem(`favorites${props.e.article}`)
             }
        }
    
        return (
            <div className='card'>
                <div>
                    <p>Article code:{e.article}</p>
                    <img className='card__image' alt='product-img' src={e.image} ></img>
                </div>

                <div className='card__description'>
                    {!localStorage.getItem(`favorites${props.e.article}`) && <span onClick ={favorites.bind(this)} className='card__description--star'>&#9734;</span>}
                    {localStorage.getItem(`favorites${props.e.article}`) &&<span onClick ={favorites.bind(this)} className='card__description--star'>&#9733;</span>}
                    <h3 className='card__description--name'>{e.name}</h3>
                    <p className='card__description--descriptionproduct'>{e.description}</p>
                    <p  className='card__description--color'> Color:
                        <span style={{'color': `${e.color}`}} >{e.color} </span>
                    </p>
                    <div style={{'backgroundColor': `${e.color}`}} className='ololo'></div>
                    <p className='card__description--price'> &#8372;{e.price}  </p>

                </div>
                {!localStorage.getItem(`cart${props.e.article}`) && <Button
                    className = 'btn'
                    key={0}
                    text={'Add to cart'}
                    onClick = {openFirstModal}
                    addToCart={()=> addToCart(e.article) }
                />}

                {localStorage.getItem(`cart${props.e.article}`) && <Button
                    className = 'btn-delete'
                    key={1}
                    text={'Delete from cart'}
                    onClick = {openSecondModal}
                />}

                {modalochka}
                {modalochka2}
            </div>
            
           
        )
}

export default Card 