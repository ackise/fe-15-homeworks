import React from 'react'
import './Modalstyle.scss'

 const Modal = (props)=> {
        const {closeButton,header,body,actions,type} = props
        let clsbtn = [`modal__close__${props.type}`]
        let exBtn = (
            <button className = {clsbtn} onClick = {props.onClose} >X</button>
        )
        let cless = ['modal' +' '+ `modal__${props.type}`]
        let title = [`modal__header--${props.type}`]
    
        let modal = (
            <>
            <div className ={cless}  >
                <div className = {title}  >
                    <h3 >{header}</h3>
                    <span className = 'modal__closebtn' >{closeButton && exBtn}</span></div>
                <div>
                    <p>{body}</p>
                </div>
                <div className = 'modal__footer' >{actions}</div>
                
            </div>
            <div className = 'overlay' onClick = {props.onClose}></div>
            </>
        );
        if (!props.isOpen){
            modal = null
        }
        if(!props.closeButton){
            exBtn = null
        }
        return (
         <>
            {modal}
            
         </>
        )
}
export default Modal