import React from 'react';
import CardWrap from '../../components/CardWrap/CardWrap'
import {connect} from 'react-redux'

const Catalog = (props) =>{
    const {stuff} = props

    return (
        <>
            <h2>Our products</h2>
            <CardWrap stuff={stuff}></CardWrap>
        </>
    )
}
const mapStoretoProps = (state)=>({
    stuff: state.products.products
 })
export default connect(mapStoretoProps) (Catalog)