import React from 'react';
import CardWrap from '../../components/CardWrap/CardWrap'
import { connect } from 'react-redux';

const Cart = (props) =>{
    const {stuff} = props
     const stuffCart = stuff.filter(e=>localStorage.getItem(`cart${e.article}`))

    return (
        <>
            <h2>Producrs in cart</h2>
            <CardWrap stuff={stuffCart}></CardWrap>
        </>
    )
}
const mapStoretoProps = (state)=>({
    stuff: state.products.products
 })
export default connect(mapStoretoProps) (Cart)
