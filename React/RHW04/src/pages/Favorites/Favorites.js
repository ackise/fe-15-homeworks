import React from 'react';
import CardWrap from '../../components/CardWrap/CardWrap'
import { connect } from 'react-redux';

const Favorites = (props) =>{
    const {stuff} = props

    const stuffFavorites = stuff.filter(e=>localStorage.getItem(`favorites${e.article}`))
  
   
    return (
        <>
            <h2>Favorites products</h2>
            <CardWrap stuff={stuffFavorites}></CardWrap>
        </>
    )
}
const mapStoretoProps = (state)=>({
    stuff: state.products.products
 })
export default connect(mapStoretoProps) (Favorites)
