import React from 'react'
import './Modalstyle.scss'
import { closeModal } from '../../reducers/modalActions'
import {useDispatch} from 'react-redux'


 const Modal = (props)=> {
        const dispatch = useDispatch()
        const {closeButton,header,body,actions,type} = props
        let clsbtn = [`modal__close__${props.type}`]
        let exBtn = (
            <button className = {clsbtn} onClick ={()=>dispatch(closeModal())} >X</button>
        )
        let cless = ['modal' +' '+ `modal__${props.type}`]
        let title = [`modal__header--${props.type}`]
    
        let modal = (
            <>
            <div className ={cless}  >
                <div className = {title}  >
                    <h3 >{header}</h3>
                    <span className = 'modal__closebtn' >{closeButton && exBtn}</span></div>
                <div>
                    <p>{body}</p>
                </div>
                <div className = 'modal__footer' >{actions}</div>
                
            </div>
            <div className = 'overlay' onClick ={()=>dispatch(closeModal())}></div>
            </>
        );
        if (!props.isOpen){
            modal = null
        }
        if(!props.closeButton){
            exBtn = null
        }
        return (
         <>
            {modal}
            
         </>
        )
}
export default Modal