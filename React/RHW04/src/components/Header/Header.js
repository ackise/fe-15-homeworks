import React from 'react'
import './Header.scss'
import{NavLink} from 'react-router-dom'

const Header = ()=> {
        return (
            <div className = 'header'>
                <div className = 'header__logo'>
                    <div className = 'header__logo__logotip' >
                        <img  src='../drones-img/logo.jpg' alt='logo' ></img>
                    </div>
                    <div className = 'header__logo__favcart'>
                        <NavLink className = 'header__logo__favcart__fav' to='/catalog'>Catalog</NavLink>
                        <NavLink className = 'header__logo__favcart__fav' to='/favorites'>Favorites</NavLink>
                        <NavLink className = 'header__logo__favcart__fav' to='/cart'>Cart</NavLink>
                    </div>
                </div>
          <div className = 'header__menu' >
            <div className = 'header__menu--items'>
              <img  src='../drones-img/mavic-logo.jpg'alt='mavic-logo' ></img>
              <span className = 'header__menu--link'>Mavic</span>
            </div>
            <div className = 'header__menu--items'>
              <img src='../drones-img/osmo-logo.jpg' alt='osmo-logo'></img>
              <span className = 'header__menu--link' >Osmo</span>
            </div>
            <div className = 'header__menu--items'>
              <img src='../drones-img/inspire2-logo.jpg' alt='inspire-logo'></img>
              <span className = 'header__menu--link' >Inspire</span>
            </div>
            <div className = 'header__menu--items'>
              <img src='../drones-img/matrice-logo.jpg' alt='matrice-logo'></img>
              <span className = 'header__menu--link' >For Pro</span>
            </div>
          </div>
        </div>
        )
}
export default Header