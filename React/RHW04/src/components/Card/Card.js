import React, {useState } from 'react'
import './Card.scss';
import Button from '../Btn/Button'
import Modal from '../Modal/Modal'
import {useDispatch, connect} from 'react-redux'
import { addToFavorites, removeFromFavorites, addToCart, deleteFromCart } from '../../actions/productsActions';
import { openModal,closeModal } from '../../reducers/modalActions';



const  Card = (props) => {

    const dispatch = useDispatch()
   
        const {e,modal} = props
        
        console.log(props)
        return (
            <div className='card'>
                <div>
                    <p>Article code:{e.article}</p>
                    <img className='card__image' alt='product-img' src={e.image} ></img>
                </div>

                <div className='card__description'>
                    {!localStorage.getItem(`favorites${e.article}`) && <span onClick ={()=> dispatch(addToFavorites(e.article))} className='card__description--star'>&#9734;</span>}
                    {localStorage.getItem(`favorites${e.article}`) &&<span onClick ={()=> dispatch(removeFromFavorites(e.article))} className='card__description--star'>&#9733;</span>}
                    <h3 className='card__description--name'>{e.name}</h3>
                    <p className='card__description--descriptionproduct'>{e.description}</p>
                    <p  className='card__description--color'> Color:
                        <span style={{'color': `${e.color}`}} >{e.color} </span>
                    </p>
                    <div style={{'backgroundColor': `${e.color}`}} className='ololo'></div>
                    <p className='card__description--price'> &#8372;{e.price}  </p>

                </div>
                {!localStorage.getItem(`cart${e.article}`) && <Button
                    className = 'btn'
                    key={0}
                    text={'Add to cart'}
                    onClick = {()=>dispatch(openModal())}
                    addToCart={()=> dispatch(addToCart(e.article)) }
                />}

                {localStorage.getItem(`cart${e.article}`) && <Button
                    className = 'btn-delete'
                    key={1}
                    text={'Delete from cart'}
                    onClick ={()=>dispatch(openModal())}
                    addToCart={()=>dispatch(deleteFromCart(e.article))}
                />}
                

            {modal.isOpen && <Modal
            isOpen={modal.isOpen}
            header={'Success'}
            body={'Action completed successfully'}
            type={'first-modal'}
            closeButton={true}
            activeMiodal={'firstModal'}
            actions={[
                      <Button
                      key={0}
                      text={'OK'}
                      backgroundColor={'whitesmoke'}
                      color={'#44a8f2'}
                      onClick ={()=>dispatch(closeModal())}
                  />,
                    ]}
            ></Modal> }
            </div>
 


           
        )
}
const mapStoretoProps = (state)=>({
    modal: state.modal
 })
export default connect(mapStoretoProps) (Card)