import { combineReducers } from "redux";
import stuff from "./productsReducer";
import modalReducer from "./modalReducer";

const routReducer = combineReducers({
    products: stuff,
    modal:modalReducer,
})

export default routReducer