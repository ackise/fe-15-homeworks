
const initialStore = {
  products: []
}

const stuff = (state = initialStore,action) =>{
    switch(action.type){
      case 'SAVE_PRODUCTS':
        return {...state, products:action.payload}
      case 'ADD_FAVORITES':
        localStorage.setItem(`favorites${action.payload}`, 'true')
        return {...state,
        products: state.products.map(e =>{ return  e.article === action.payload ? {...e} : e})
        }
      case 'REMOVE_FROM_FAVORITES':
        localStorage.removeItem(`favorites${action.payload}`)
        return {...state,
          products: state.products.map(e =>{ return  e.article === action.payload ? {...e} : e})
        }
      case 'ADD_TO_CART':
        localStorage.setItem(`cart${action.payload}`, 'true')
        return {...state,
        products: state.products.map(e =>{ return  e.article === action.payload ? {...e} : e})
        }
      case 'DELETE_FROM_CART':
        localStorage.removeItem(`cart${action.payload}`)
        return{...state,
        products: state.products.map(e =>{ return  e.article === action.payload ? {...e} : e})
        }
      default: return  state;
    }
   
  }
  export default stuff 