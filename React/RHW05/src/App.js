import React, {useEffect} from 'react';
import './App.css';
import Header from './components/Header/Header';
import Routes from './router/Routes';
import { useDispatch } from 'react-redux';
import {loadProducts} from './actions/productsActions'

const App = () => {
  const dispatch = useDispatch()
  useEffect(()=>{
    dispatch(loadProducts())
    })

    return (
   
      <div className="App">
        <Header/>
        <Routes/>
        
      </div>
    );  
}

export default App;

