
import axios from 'axios'

export const loadProducts = () => (dispatch) => {
    dispatch({type: 'LOADING_PRODUCTS' , payload : true})
    axios('/stuff.json')
            .then(res => {
                const checkProducts = res.data.map (e =>{
                    return localStorage.getItem(`cart${e.article}`) ? {...e,inCart:true}:e
                })
              dispatch({type:'SAVE_PRODUCTS' , payload: checkProducts})
              dispatch({type:'LOADING_PRODUCTS' , payload: false})
            })
    
  }
export const addToFavorites = (id) => (dispatch) => {
    dispatch({type: 'ADD_FAVORITES', payload: id});
    localStorage.setItem(`favorites${id}`, true)
}
export const removeFromFavorites = (id) => (dispatch) => {
    dispatch({type: 'REMOVE_FROM_FAVORITES', payload: id});
    localStorage.removeItem(`favorites${id}`)
}
export const addToCart = (id) => (dispatch) => {
    localStorage.setItem(`cart${id}`, true)
    dispatch({type: 'ADD_TO_CART', payload: id});
}

export const deleteFromCart = (id) => (dispatch) => {
    localStorage.removeItem(`cart${id}`)
    dispatch({type: 'DELETE_FROM_CART', payload: id});
}

export const deleteFromCartOrder = (stuffInCart) => (dispatch) => {
    stuffInCart.forEach(e =>localStorage.removeItem(`cart${e}`) )
    dispatch(loadProducts());
    
}