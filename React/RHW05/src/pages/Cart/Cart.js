import React from 'react';
import CardWrap from '../../components/CardWrap/CardWrap'
import { connect, useDispatch } from 'react-redux';
import Forma from '../../components/Form/Form';
import Modal from '../../components/Modal/Modal';
import Button from '../../components/Btn/Button';
import { closeModal } from '../../reducers/modalActions';
import './Cart.scss'


const Cart = (props) =>{
    const {stuff,modal} = props
     const stuffCart = stuff.filter(e => e.inCart)
     const dispatch = useDispatch()

    return (
        <div className='wrapp'>
            {stuffCart.length < 1 && <h2>Your cart is empty</h2> } 
            
            {stuffCart.length > 0 &&  <Forma stuff={stuffCart}></Forma>}

            {modal.isOpen && <Modal
            isOpen={modal.isOpen}
            header={'Success'}
            body={'Your order has been added'}
            type={'first-modal'}
            closeButton={true}
            activeMiodal={'firstModal'}
            actions={[
                      <Button
                      key={0}
                      text={'OK'}
                      backgroundColor={'whitesmoke'}
                      color={'#44a8f2'}
                      onClick ={()=>dispatch(closeModal())}
                  />,
                    ]}
            ></Modal> }

       
            <CardWrap stuff={stuffCart}></CardWrap>
            
            
           
        </div>
    )
}
const mapStoretoProps = (state)=>({
    stuff: state.products.products,
    modal: state.modal
 })
export default connect(mapStoretoProps) (Cart)
