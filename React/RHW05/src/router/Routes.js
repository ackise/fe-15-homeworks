import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Favorites from '../pages/Favorites/Favorites';
import Cart from '../pages/Cart/Cart'
import Catalog from '../pages/Catalog/Catalog';

const Routes = (props) => {
    return(
        <>
            <Switch>
                <Route exact path='/' render={() => <Redirect to='/catalog' />} />
                <Route exact path='/catalog' render ={()=> <Catalog></Catalog>}></Route>
                <Route exact path='/cart' render={()=> <Cart></Cart>} ></Route>
                <Route exact path='/favorites' render={()=> <Favorites></Favorites>} ></Route>
            </Switch>
        </>
    )
}
export default Routes