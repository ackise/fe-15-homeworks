
const initialStore = {}
const  formReducer = (state = initialStore,action) =>{
    switch(action.type){
        case 'SAVE_FORM':
            return {...state, ...action.payload}  
        default: return  state;
    }
}
export default formReducer