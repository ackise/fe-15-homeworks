import { combineReducers } from "redux";
import stuff from "./productsReducer";
import modalReducer from "./modalReducer";
import formReducer from './formReducer'

const routReducer = combineReducers({
    products: stuff,
    modal:modalReducer,
    form:formReducer
})

export default routReducer