import React from 'react'
import PropTypes from 'prop-types'
import './Btn.scss';

const  Button = (props) => {

        const {text,backgroundColor,onClick,addToCart,color} = props
     
        return (
           <button
            className = 'btn'
            style={{'backgroundColor': backgroundColor},{'color': color}}
            onClick={()=>{
                onClick();
                if(addToCart){
                    addToCart();
                } 
            }}
            >
            {text}
            </button>
        )
}
Button.propTypes = {
    backgroundColor: PropTypes.string,
    className:PropTypes.string,
    color:PropTypes.string,
    onClick: PropTypes.func,
    text: PropTypes.string
  };
export default Button