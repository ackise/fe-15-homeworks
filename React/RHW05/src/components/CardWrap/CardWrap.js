import React from 'react'
import Card from '../Card/Card'
import './CardWrap.scss'

const CardWrap = (props)=> {
        const {stuff} = props
        return (
            <div className = 'cardwrap'>
               {stuff.map(e => <Card e={e} key={e.article}/>)}
            </div>
        )
}

export default CardWrap