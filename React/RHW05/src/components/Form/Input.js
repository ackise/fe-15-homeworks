import React from 'react';
import { useField } from 'formik';


function OrderFormInput(props) {
    const { name, ...rest } = props;
    const [field, meta] = useField(name);

    return (
        <>
            <input {...field} {...rest} />
            {
                meta.touched && meta.error && (
                    <span className='form-error'>{meta.error}</span>
                )
            }
        </>

    );
}

export default OrderFormInput;

