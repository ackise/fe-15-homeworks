import { Formik, Field, Form, ErrorMessage, connect } from 'formik';
import React, { useEffect } from 'react'
import * as Yup from 'yup';
import './Forma.scss'
import { deleteFromCartOrder } from '../../actions/productsActions';
import { useDispatch, useSelector } from 'react-redux';
import { openModal } from '../../reducers/modalActions';
import { saveForm } from '../../actions/formActions';

const phoneRegEx = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
const nameRegEx = /^[a-z ,.'-]+$/i

const formSchema = Yup.object().shape({
    firstName: Yup
        .string()
        .matches(nameRegEx,'First name is invalid')
        .min(2, 'First name must be longer then 2 symbols')
        .max(20, 'First name must be shorter then 20 symbols')
        .required('This field is required'),
    lastName: Yup
        .string()
        .matches(nameRegEx,'Last name is invalid')
        .min(2, 'Last name must be longer then 2 symbols')
        .max(20, 'Last name must be shorter then 20 symbols')
        .required('This field is required'),
    age: Yup
        .number()
        .min(18, 'You must be over 18 years old')
        .required("This field is required"),
    address: Yup
        .string()
        .required("This field is required"),
    phone: Yup
        .string()
        .matches(phoneRegEx, 'Phone number is not valid')
        .required("This field is required"),
    


})



const Forma = (props)=>{
    const {stuff} = props
    const stuffInCart = stuff.map(e => e.article)
    const dispatch = useDispatch()
    const formValues = useSelector(state => state.form)
    const handleForm = (values)=>{
        dispatch((saveForm(values)))
        console.log(formValues)
     }
 


    return(
    <div className='forma-wrapper'>
        <h1>Make your order</h1>
        <Formik
        
            initialValues={{
                firstName: formValues.firstName,
                lastName: formValues.lastName,
                phone: formValues.phone,
                age:formValues.age,
                address:formValues.address
            }}
            validationSchema={formSchema}
            onSubmit={async values => {
                await new Promise(r => setTimeout(r, 500));
                console.log(JSON.stringify(values, null, 2));
                console.log(stuff)               
                dispatch(deleteFromCartOrder(stuffInCart))
                dispatch(openModal())
                
              }}
              handleChange={handleForm}
            
            >
                {({errors, touched, values})=>(
                    
                <>

                
              
                <Form className='forma' >
                    <label className='label' htmlFor="firstName">First Name</label>
                <Field onBlur={() => handleForm(values)} id="firstName" type='text' className='input'  name="firstName" placeholder="Name" />{errors.firstName && touched.firstName ?(<div>{errors.firstName}</div>) : null}
              
                    <label className='label' htmlFor="lastName">Last Name</label>
                    <Field onBlur={() => handleForm(values)}id="lastName" name="lastName" className='input' placeholder="Last name" />{errors.lastName && touched.lastName ?(<div>{errors.lastName}</div>) : null}
             
    
                    <label className='label' htmlFor="phone">Phone</label>
                    <Field onBlur={() => handleForm(values)}id="phone" name="phone" className='input'placeholder="Phone" />{errors.phone && touched.phone ? (<div>{errors.phone}</div>) : null}
           
    
                    <label className='label' htmlFor="age">Age</label>
                    <Field onBlur={() => handleForm(values)}id="age" name="age" className='input'placeholder="18" />{errors.age && touched.age ?(<div>{errors.age}</div>) : null}
       
    
                    <label className='label' htmlFor="address">Address</label>
                    <Field onBlur={() => handleForm(values)}id="address" name="address" className='input' placeholder="Address" />{errors.address && touched.address ?(<div>{errors.address}</div>) : null}
         

                    <button className='orderbtn' type="submit">Make order</button>
                </Form>
                    </>
                )}
            
        </Formik>
    </div>
    )
    

}



export default Forma