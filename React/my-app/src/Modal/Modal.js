import React, { Component } from 'react'
import './Modalstyle.scss'

export default class Modal extends Component {
    render() {
        const {closeButton,header,body,actions,type} = this.props
       
        
        let exBtn = (
            <button onClick = {this.props.onClose} >X</button>
        )
        let cless = ['modal' +' '+ `modal__${this.props.type}`]
        let title = [`modal__header--${this.props.type}`]
        let modal = (
            <div className ={cless}  >
                <div className = {title}  >
                    <h3 >{header}</h3>
                    <span className = 'modal__closebtn' >{closeButton && exBtn}</span></div>
                <div>
                    <p>{body}</p>
                </div>
                <div className = 'modal__footer' >{actions}</div>
                <div className = 'overlay' onClick = {this.props.onClose} ></div>
            </div>

        );
        if (!this.props.isOpen){
            modal = null
        }
        if(!this.props.closeButton){
            exBtn = null
        }
        
        return (
            
         <div>
             {modal}
         </div>
        )
    }
}
