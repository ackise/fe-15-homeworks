import React, { Component } from 'react';
import Modal from './Modal/Modal'
import Button from './Btn/Button'
import './App.css';

class App extends Component {
  state = {
    isOpen:false,
      firstModal:{
        header: 'First Modal',
        body: 'File downloaded',
        type:'first-modal',
        closeButton: true,
        actions: [
          <Button
          key={0}
          text={'OK'}
          backgroundColor={'green'}
      />,
        ]
      },
      secondModal:{
        header:'Do you want delete this file?',
        body:'Once you delete this file, it won’t be possible to undo this action.Are you sure you want to delete it?',
        type:'second-modal',
        closeButton: true,
        actions: [
          <Button
          key={0}
          text={'OK'}
          backgroundColor={'green'}
          />,
          <Button
              key={1}
              text={'Cancel'}
              backgroundColor={'red'}
          />
          ]
      },
      activeMiodal: null
    }
 
  
  render(){
  
  const choose = this.state[this.state.activeMiodal]
   const modalochka = choose && <Modal 
   isOpen= {this.state.isOpen} 
   onClose = {(e) => this.setState({isOpen: false})} 
   header = {choose.header}
   body = {choose.body}
   actions = {choose.actions}
   closeButton = {choose.closeButton}
   type = {choose.type}
   ></Modal>

  
    return (
      
      <div className="App">
       <Button 
        text = {'Open First Modal'}
        backgroundColor={'green'}
        onClick = {this.openFirstModal.bind(this)}
        />
        <Button 
        text = {'Open Second Modal'}
        backgroundColor={'red'}
        onClick = {this.openSecondModal.bind(this) }
        />
        {modalochka}
      </div>
     
      
    );

  }
  openFirstModal() {
    this.setState({
      isOpen:true,
      activeMiodal: 'firstModal'
    })
  }
  openSecondModal(){
    this.setState({
      isOpen:true,
      activeMiodal: 'secondModal'

    })
  }
}

export default App;

