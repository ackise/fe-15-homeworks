import React, { Component } from 'react'
import PropTypes from 'prop-types'



export default class Button extends Component {

    
    render() {
        const {text,backgroundColor,onClick} = this.props
        return (
           <button
            style={{'backgroundColor': backgroundColor}}
            onClick={onClick}
            >
            {text}
            </button>
        )
    }
}
Button.propTypes = {
    backgroundColor: PropTypes.string,
    onClick: PropTypes.func,
    text: PropTypes.string
  };
  