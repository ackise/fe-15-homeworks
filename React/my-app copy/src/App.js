import React, { Component } from 'react';
import Modal from './Modal/Modal'
import Button from './Btn/Button'
import './App.css';
import CardWrap from './CardWrap/CardWrap';
import Header from './Header/Header';

class App extends Component {
  state = {
    isOpen:false,
      firstModal:{
        header: 'Success',
        body: 'Item was added to cart',
        type:'first-modal',
        closeButton: true,
        actions: [
          <Button
          key={0}
          text={'OK'}
          backgroundColor={'whitesmoke'}
          color={'#44a8f2'}
          onClick={(e) => this.setState({isOpen: false})}
      />,
        ]
      },
      secondModal:{
        header:'Do you want delete this file?',
        body:'Once you delete this file, it won’t be possible to undo this action.Are you sure you want to delete it?',
        type:'second-modal',
        closeButton: true,
        actions: [
          <Button
          key={0}
          text={'OK'}
          backgroundColor={'green'}
          />,
          <Button
              key={1}
              text={'Cancel'}
              backgroundColor={'red'}
          />
          ]
      },
      activeMiodal: null,
      stuff:null
    }
    componentDidMount() {
        fetch('/stuff.json')
          .then(res => res.json())
          .then(data => this.setState({ stuff: data }))
          
    }
  
  render(){
  
  const choose = this.state[this.state.activeMiodal]
   const modalochka = choose && <Modal 
   isOpen= {this.state.isOpen} 
   onClose = {(e) => this.setState({isOpen: false})} 
   header = {choose.header}
   body = {choose.body}
   actions = {choose.actions}
   closeButton = {choose.closeButton}
   type = {choose.type}
   ></Modal>

    return (
      
      <div className="App">
        <Header/>
        {modalochka}
        {this.state.stuff && <CardWrap openFirstModal={this.openFirstModal.bind(this)} addToCart={this.addToCart.bind(this)} stuff={this.state.stuff}>
        </CardWrap>}
      </div>
    );

  }
 
  openFirstModal() {
    this.setState({
      isOpen:true,
      activeMiodal: 'firstModal'
    })
  }
  openSecondModal(){
    this.setState({
      isOpen:true,
      activeMiodal: 'secondModal'

    })
  }
  addToCart(id) {
    localStorage.setItem(`cart${id}`, true)
  }
}

export default App;

