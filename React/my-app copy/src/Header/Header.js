import React, { Component } from 'react'
import './Header.scss'

export default class Header extends Component {
    render() {
        return (
            <div className = 'header'>
                <div className = 'header__logo'>
                    <div className = 'header__logo__logotip' >
                        <img  src='../drones-img/logo.jpg' alt='logo' ></img>
                    </div>
                    <div className = 'header__logo__favcart'>
                        <span className = 'header__logo__favcart__fav'>Favorites</span>
                        <span className = 'header__logo__favcart__fav'>Cart</span>
                    </div>
                </div>
          <div className = 'header__menu' >
            <div className = 'header__menu--items'>
              <img  src='../drones-img/mavic-logo.jpg'alt='mavic-logo' ></img>
              <span className = 'header__menu--link'>Mavic</span>
            </div>
            <div className = 'header__menu--items'>
              <img src='../drones-img/osmo-logo.jpg' alt='osmo-logo'></img>
              <span className = 'header__menu--link' >Osmo</span>
            </div>
            <div className = 'header__menu--items'>
              <img src='../drones-img/inspire2-logo.jpg' alt='inspire-logo'></img>
              <span className = 'header__menu--link' >Inspire</span>
            </div>
          </div>
        </div>
        )
    }
}
