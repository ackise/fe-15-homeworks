import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './Btn.scss';



export default class Button extends Component {

    
    render() {
        const {text,backgroundColor,onClick,addToCart,color} = this.props
        return (
           <button
            className = 'btn'
            style={{'backgroundColor': backgroundColor},{'color': color}}
            onClick={()=>{
                onClick();
                if(addToCart){
                    addToCart();
                } 
            }}
            >
            {text}
            </button>
        )
    }
}
Button.propTypes = {
    backgroundColor: PropTypes.string,
    color:PropTypes.string,
    onClick: PropTypes.func,
    text: PropTypes.string
  };
  