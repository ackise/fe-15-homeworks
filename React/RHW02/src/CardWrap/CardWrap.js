import React, { Component } from 'react'
import Card from '../Card/Card'

import './CardWrap.scss'

export default class CardWrap extends Component {
    render() {
        const {stuff,openFirstModal,addToCart} = this.props
        return (
            <div className = 'cardwrap'>
               {stuff.map(e => <Card e={e} openFirstModal={openFirstModal} addToCart={addToCart}  key={e.article}/>)}
            </div>
        )
    }
}
