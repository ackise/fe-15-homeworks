import React, { Component } from 'react'
import './Card.scss';
import Button from '../Btn/Button'

export default class Card extends Component {
    state = {
        inFavorites:false
    }

    render() {
        const { e ,modalochka,openFirstModal,addToCart} = this.props
        
        return (
            <div className='card'>
                <div>
                    <p>Article code:{e.article}</p>
                    <img className='card__image' alt='product-img' src={e.image} ></img>
                </div>

                <div className='card__description'>
                    {!localStorage.getItem(`favorites${this.props.e.article}`) && <span onClick ={this.favorites.bind(this)} className='card__description--star'>&#9734;</span>}
                    {localStorage.getItem(`favorites${this.props.e.article}`) &&<span onClick ={this.favorites.bind(this)} className='card__description--star'>&#9733;</span>}
                    <h3 className='card__description--name'>{e.name}</h3>
                    <p className='card__description--descriptionproduct'>{e.description}</p>
                    <p  className='card__description--color'> Color:
                        <span style={{'color': `${e.color}`}} >{e.color} </span>
                    </p>
                    <div style={{'backgroundColor': `${e.color}`}} className='ololo'></div>
                    <p className='card__description--price'> &#8372;{e.price}  </p>

                </div>
                <Button
                    className = 'btn'
                    key={0}
                    text={'Add to cart'}
                    onClick = {openFirstModal}
                    addToCart={()=> addToCart(e.article) }
                />
                {modalochka}
            </div>
           
        )
        

    }
    favorites() {
        this.setState((actualState) => {
            return {
                inFavorites:!actualState.inFavorites
            }})

         if(!localStorage.getItem(`favorites${this.props.e.article}`)) {
             localStorage.setItem(`favorites${this.props.e.article}`, true)
         } else {
             localStorage.removeItem(`favorites${this.props.e.article}`)
         }

    }

}
