

let container = document.querySelector('.container');
let list = document.createElement('ul');
let listItem = document.createElement('li');
const characters = document.createElement('p')
list.append(listItem);
container.append(list);
let spinCheck = 0;


const req = new XMLHttpRequest();
req.open('GET','https://swapi.dev/api/films/');

req.onload = function () {
    if(req.status === 200){
        let data = JSON.parse(req.response)
        data.results.forEach(element => {
            const title = document.createElement('h2');
            const episodId = document.createElement('p');
            const opening_crawl = document.createElement('p');
            
            title.innerText = `${element.title}`;
            episodId.innerText =`${element.episode_id}`;
            opening_crawl.innerText = `${element.opening_crawl}`;
            
            listItem.append(title);
            listItem.append(characters);
            listItem.append(episodId);
            listItem.append(opening_crawl);

            
            element.characters.forEach(elements=>{
                const req2 = new XMLHttpRequest();
                req2.open('GET', elements);
                req2.onload = function (){
                    const data2 = JSON.parse(req2.response);
                    if(req2.status === 200){
                           title.after(`${data2.name};`);
                    }
                }
                req2.send(); 
                
            })
           
        });

    }
}
req.send();

function spinner(){
    const spin = document.createElement('div');
    spin.classList.add('spin');
    characters.after(spin);
    characters.classList.add('none');
    spin.style.display = 'none'
    characters.classList.remove('none');
}