

let container = document.querySelector('.container');
let list = document.createElement('ul');
let listItem = document.createElement('li');
const characters = document.createElement('p')
list.append(listItem);
container.append(list);


const films = fetch('https://swapi.dev/api/films/')
.then(response => response.json())
.then(json => {
    json.results.forEach(element => {
        const title = document.createElement('h2');
        const episodId = document.createElement('p');
        const opening_crawl = document.createElement('p');
        
        title.innerText = `${element.title}`;
        episodId.innerText =`${element.episode_id}`;
        opening_crawl.innerText = `${element.opening_crawl}`;
        
        listItem.append(title);
        listItem.append(characters);
        listItem.append(episodId);
        listItem.append(opening_crawl);

        const info = []
        element.characters.forEach( link => {
            console.log(link)
            const character = fetch(link).then(response => response.json())
            info.push(character)
        })
        Promise.all(info)
        .then(values => {
            console.log(values)
            values.forEach(el => {
                title.after(el.name + ', ')
            })
        } )
    
    })
     
})

