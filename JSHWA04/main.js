
class Column {
    constructor(title) {
        this.title = title;
        this.sortBtn = document.createElement('button');
        this.taskBtn = document.createElement('button');
        this.card = document.createElement('div');
        this.cardList = [];
    }
    createTrello() {
        this.addColumn();
        this.addCard();
        this.newTesk();
        this.sortList();
    }
    addColumn() {

        const wrapper = document.createElement('div');
        document.querySelector('.container').prepend(wrapper);
        wrapper.classList.add('wrapper');

    }
    newTesk() {
        this.taskBtn.addEventListener('click', () => {
            const block = document.createElement('div');
            block.classList.add('block')
            block.setAttribute('draggable', true);
            this.taskBtn.before(block)

            const textArea = document.createElement('textarea')
            textArea.classList.add('text');
            block.appendChild(textArea);
            this.cardList.push(block);


        })
    }
    addCard() {

        document.querySelector('.wrapper').appendChild(this.card);
        this.card.classList.add('card');
        this.card.id = "list"

        const block = document.createElement('div');
        block.classList.add('block')
        block.setAttribute('draggable', true);
        this.card.appendChild(block)

        const textArea = document.createElement('textarea')
        textArea.classList.add('text');
        block.appendChild(textArea);
        this.cardList.push(block);

        this.taskBtn.innerText = "New Task"
        this.taskBtn.classList.add('taskbtn')
        this.card.append(this.taskBtn);

        const tasksListElement = document.querySelector(".card");
        const taskElements = tasksListElement.querySelectorAll(".block");

        tasksListElement.addEventListener("dragstart", (evt) => {
            evt.target.classList.add("selected");
        });

        tasksListElement.addEventListener("dragend", (evt) => {
            evt.target.classList.remove("selected");
        });

        tasksListElement.addEventListener("dragover", (evt) => {
            evt.preventDefault();
            const activeElement = tasksListElement.querySelector(".selected");
            const currentElement = evt.target;
            const isMoveable =
                activeElement !== currentElement &&
                currentElement.classList.contains("block");

            if (!isMoveable) return;

            const nextElement = getNextElement(evt.clientY, currentElement);

            if (
                (nextElement && activeElement === nextElement.previousElementSibling) ||
                activeElement === nextElement
            ) {
                return;
            }

            tasksListElement.insertBefore(activeElement, nextElement);
        });

        const getNextElement = (cursorPosition, currentElement) => {
            const currentElementCoord = currentElement.getBoundingClientRect();
            const currentElementCenter =
                currentElementCoord.y + currentElementCoord.height / 2;
            const nextElement =
                cursorPosition < currentElementCenter
                    ? currentElement
                    : currentElement.nextElementSibling;
            return nextElement;
        };




    }
    clearList() {
        this.cardList.forEach(e => {
            e.remove()
        })

    }

    showList() {
        this.cardList.forEach(e => {
            this.taskBtn.before(e)
        })

    }

    sortList() {
        this.sortBtn.innerText = "Sort"
        this.sortBtn.classList.add('sortbtn')
        this.card.append(this.sortBtn);

        this.sortBtn.addEventListener('click', () => {
            this.cardList.sort((a, b) => {
                if (a.children[0].value > b.children[0].value) {
                    return 1;
                }
                if (a.children[0].value < b.children[0].value) {
                    return -1;
                }
                return 0;
            })
            this.clearList();
            this.showList();
        })
    }




}

const createColumn = document.createElement('button');
createColumn.innerHTML = 'Create Column';
document.querySelector('body').prepend(createColumn);

createColumn.addEventListener('click', () => {
    let newTrello = new Column('New Trello');
    newTrello.createTrello();
})







