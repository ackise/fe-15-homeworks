const container = document.querySelector('.container');

class App {
    constructor(){
        this.newCardBtn = document.createElement('button')
    }
    start(){
        this.getInfo();
        this.newCard();
    }
    getInfo(){
        const users = fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json());

        const posts = fetch('https://jsonplaceholder.typicode.com/posts')
        .then(response => response.json());

        Promise.all([posts,users])
            .then(values  => {
                console.log(values)
               const posets = values[0].map( post => {
                    const users = values[1].find(user => {
                         return user.id === post.userId
                    })
                    let name = users.name;
                    let email = users.email
                    return {...post,name,email}
                })
                posets.forEach(element => {
                    let newPost = new Cards(element)
                    newPost.start()
                });
            })
    }
    modalWindow(){
       
            let modal = document.createElement('div');
            modal.classList.add('modalblock')
            let newTitle = document.createElement('input')
            newTitle.setAttribute('placeholder','Text your title')
            newTitle.classList.add('modal-title')
            let newText = document.createElement('textarea')
            newText.setAttribute('placeholder','Text your post')
            newText.classList.add('modal-text')
            
            container.append(modal)
            let closebtn = document.createElement('button')
            closebtn.classList.add('closebtn')
            closebtn.innerText = 'X'

            
            let savebtn = document.createElement('button')
            savebtn.classList.add('savebtn')
            savebtn.innerText = 'Save Post'

            modal.append(newTitle,newText,closebtn,savebtn);

            closebtn.addEventListener('click',onRemove);
            function onRemove(){
                modal.remove()
                closebtn.removeEventListener('click',onRemove)
            }

            savebtn.addEventListener('click',onSave.bind(this));
            function onSave(){
                fetch('https://jsonplaceholder.typicode.com/posts', {
                method: 'POST',
                body: JSON.stringify({
                title: newTitle.value,
                body: newText.value,
                userId: 1
                }),
                headers: {
                "Content-type": "application/json; charset=UTF-8"
                }
            })
            .then(response => response.json())
            .then(json => {
                let newPost = json
                newPost.name = 'Leanne Graham'
                newPost.email = 'Sincere@april.biz'
                let newCard = new Cards(newPost)
                newCard.start();
                modal.remove()
                savebtn.removeEventListener('click',onSave.bind(this));
            })
                        }

        
    }
    newCard(){
        this.newCardBtn.innerText = 'New Post'
        this.newCardBtn.classList.add('newcardbtn')
        container.before(this.newCardBtn);
        this.newCardBtn.addEventListener('click',()=>{
            this.modalWindow()
        })
   
    }
    sortCards(){
        return posets.sort((a,b)=> a.id > b.id)
    }

}

class Cards {
    constructor({title,name,body,email,id,userId}){
      
        this.title = title;
        this.name = name;
        this.body = body;
        this.email = email;
        this.id = id;
        this.userId = userId;
        this.editBtn = document.createElement('button');
        this.deleteBtn = document.createElement('button');
        this.titles = document.createElement('h2')
        this.text = document.createElement('span')

    }

    start(){
        this.createPost();
        this.deletePost();
        this.editPost();
    }
    createPost(){
        const box = document.createElement('div')
        this.titles.innerText = this.title
        this.titles.classList.add('titles')
        const names = document.createElement('span')
        names.innerText = this.name
        names.classList.add('name')
        const emails = document.createElement('span')
        emails.innerText = this.email
        emails.classList.add('email')
        this.text.innerText = this.body
        this.text.classList.add('text')
        box.classList.add('box')
        box.append(this.editBtn);

        this.editBtn.classList.add('editBtn');
        this.editBtn.innerText = 'edit'

        this.deleteBtn.classList.add('deleteBtn');
        this.deleteBtn.innerText = 'delete post'
        box.append(this.titles,this.text,this.editBtn,this.deleteBtn,emails,names)
        container.prepend(box)

    }
    deletePost(){
        this.deleteBtn.addEventListener('click',onDelete.bind(this));
        function onDelete(){
            fetch(`https://jsonplaceholder.typicode.com/posts/${this.id}`, {
            method: 'DELETE',
            })
            this.deleteBtn.parentElement.remove();
            this.deleteBtn.removeEventListener('click',onDelete.bind(this));
        }

    }

    editModal(){
        let modal = document.createElement('div');
            modal.classList.add('modalblock')
            let newTitle = document.createElement('input')
            newTitle.setAttribute('placeholder','Text your title')
            newTitle.classList.add('modal-title')
            let newText = document.createElement('textarea')
            newText.setAttribute('placeholder','Text your post')
            newText.classList.add('modal-text')
            
            container.append(modal)
            let closebtn = document.createElement('button')
            closebtn.classList.add('closebtn')
            closebtn.innerText = 'X'

            
            let savebtn = document.createElement('button')
            savebtn.classList.add('savebtn')
            savebtn.innerText = 'Save Post'

            modal.append(newTitle,newText,closebtn,savebtn);

            closebtn.addEventListener('click',onRemove);
            function onRemove(){
                modal.remove()
                closebtn.removeEventListener('click',onRemove)
            }
            savebtn.addEventListener('click',onSave.bind(this));
            function onSave(){
                fetch(`https://jsonplaceholder.typicode.com/posts/${this.id}`, {
                    method: 'PUT',
                    body: JSON.stringify({
                      id: this.id,
                      title: newTitle.value,
                      body: newText.value,
                      userId: this.userId
                    }),
                    headers: {
                      "Content-type": "application/json; charset=UTF-8"
                    }
                  })
                  .then(response => response.json())
                  .then(json => {
                      this.titles.innerText = json.title
                      this.text.innerText = json.body
                      modal.remove();
                      savebtn.removeEventListener('click',onSave.bind(this));
                    this.editBtn.removeEventListener('click',this.editModal.bind(this))
                })
                        
                        }
                    }
            

    editPost() {
        this.editBtn.addEventListener('click',this.editModal.bind(this))

    }
        
}       

const application = new App();
application.start()


