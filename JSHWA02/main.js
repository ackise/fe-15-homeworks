
class Hamburger {
    constructor(size, stuffing) {
        if(arguments[0] === undefined){
            throw new HamburgerException('no size given')
        } else if(arguments[1] === undefined){
            throw new HamburgerException('no stuffing was given')
        } else if (arguments[0].type !== 'size') {
            throw new HamburgerException("Invalid size " + size.name)
        } else if (arguments[1].type !== 'stuffing') {
            throw new HamburgerException("Invalid stuffing " + stuffing.name)
        } else {
            this.size = size;
            this.stuffing = stuffing;
            this.toppings = [];
        }
    }

addTopping(topping) {
        if (!this.toppings.includes(topping)) {
            return this.toppings.push(topping);
        } else {
            throw new HamburgerException("duplicate topping " + topping.name)
        }
}

removeTopping(topping) {
    return this.toppings = this.toppings.filter(a => a !== topping)
}
 getToppings() {
    return this.toppings;
}
 getSize() {
    return this.size
}
 getStuffing() {
    return this.stuffing
}
 calculatePrice() {
    return this.size.price + this.stuffing.price + this.toppings.reduce((acc, val) => {
        return  acc + val.price
    }, 0)
}
 calculateCalories() {
    return this.size.calories + this.stuffing.calories + this.toppings.reduce((acc, val) => {
        return  acc + val.calories
    }, 0)
}

}

class HamburgerException extends Error {
    constructor(message) {
        super();
        this.message = message;
        this.name = 'Hamburger Exсeption'
    }
}

Hamburger.SIZE_SMALL = {price:50,calories:20,type:"size", name:'SIZE_SMALL'}
Hamburger.SIZE_LARGE = {price:100,calories:40,type:"size", name:'SIZE_LARGE'}
Hamburger.STUFFING_CHEESE = {price:10,calories:20,type:"stuffing", name:'STUFFING_CHEESE'}
Hamburger.STUFFING_SALAD = {price:20,calories:5,type:"stuffing", name:'STUFFING_SALAD'}
Hamburger.STUFFING_POTATO = {price:15,calories:10,type:"stuffing", name:'STUFFING_POTATO'}
Hamburger.TOPPING_MAYO = {price:20,calories:50, name:'TOPPING_MAYO '}
Hamburger.TOPPING_SPICE = {price:15,calories:0, name:'TOPPING_SPICE'}




const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log("Calories: %f", hamburger.calculateCalories());
console.log("Price: %f", hamburger.calculatePrice());
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log("Price with sauce: %f", hamburger.calculatePrice());
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); 
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); 




// Errors
// var h2 = new Hamburger(Hamburger.SIZE_SMALL);
// var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE); 


// var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// hamburger.addTopping(Hamburger.TOPPING_MAYO);
// hamburger.addTopping(Hamburger.TOPPING_MAYO); 

